//
//  ImageCollectionViewCell.swift
//  SpotifyView
//
//  Created by Angela Papa on 09/12/2019.
//  Copyright © 2019 Angela Papa. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var ImgImage: UIImageView!
}
